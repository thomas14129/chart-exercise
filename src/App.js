import React from 'react';
import './App.css';
import Chart from './chart.js';

function App() {
  return (
    <div className="App" width="1000" height="300">
      <Chart />
      <div className="Chart"></div>
    </div>
  );
}

export default App;
