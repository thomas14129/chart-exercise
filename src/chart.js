import React from 'react';
import ReactDOM from 'react-dom';
import { ResponsiveSankey } from '@nivo/sankey' 

class Chart extends React.Component {

    constructor() {
        super();
    }
        

  render() {
    return <ResponsiveSankey
                data={
                    {
                        nodes: [
                            { id: "pink1", color: "hsl(309, 98%, 50%)" }, 
                            { id: "pink2", color: "hsl(309, 98%, 50%)" },
                            { id: "pink3", color: "hsl(309, 98%, 50%)" },
                            { id: "pink4", color: "hsl(309, 98%, 50%)" },
                            { id: "pink5", color: "hsl(309, 98%, 50%)" },
                            { id: "pink6", color: "hsl(309, 98%, 50%)" },
                            { id: "pink7", color: "hsl(309, 98%, 50%)" },
                            { id: "pink8", color: "hsl(309, 98%, 50%)" },
                            { id: "pink9", color: "hsl(309, 98%, 50%)" },
                            { id: "pink10", color: "hsl(309, 98%, 50%)" },
                            { id: "pink11", color: "hsl(309, 98%, 50%)" },
                            { id: "pink12", color: "hsl(309, 98%, 50%)" },
                            { id: "pink13", color: "hsl(309, 98%, 50%)" },
                            { id: "pink14", color: "hsl(309, 98%, 50%)" },
                            { id: "pink15", color: "hsl(309, 98%, 50%)" },
                            { id: "pink16", color: "hsl(309, 98%, 50%)" },
                            { id: "pink17", color: "hsl(309, 98%, 50%)" },
                            { id: "pink18", color: "hsl(309, 98%, 50%)" },
                            { id: "orange1", color: "hsl(34, 98%, 50%)" },
                            { id: "orange2", color: "hsl(34, 98%, 50%)" },
                            { id: "orange3", color: "hsl(34, 98%, 50%)" },
                            { id: "orange4", color: "hsl(34, 98%, 50%)" },
                            { id: "orange5", color: "hsl(34, 98%, 50%)" },
                            { id: "orange6", color: "hsl(34, 98%, 50%)" },
                            { id: "purple1", color: "hsl(278, 74%, 44%)" },
                            { id: "purple2", color: "hsl(278, 74%, 44%)" },
                            { id: "purple3", color: "hsl(278, 74%, 44%)" },
                            { id: "purple4", color: "hsl(278, 74%, 44%)" },
                            { id: "purple5", color: "hsl(278, 74%, 44%)" },
                            { id: "purple6", color: "hsl(278, 74%, 44%)" },
                            { id: "purple7", color: "hsl(278, 74%, 44%)" },
                            { id: "purple8", color: "hsl(278, 74%, 44%)" },
                            { id: "purple9", color: "hsl(278, 74%, 44%)" },
                            { id: "purple10", color: "hsl(278, 74%, 44%)" },
                            { id: "purple11", color: "hsl(278, 74%, 44%)" },
                            { id: "purple12", color: "hsl(278, 74%, 44%)" },
                            { id: "purple13", color: "hsl(278, 74%, 44%)" },
                            { id: "purple14", color: "hsl(278, 74%, 44%)" },
                            { id: "purple15", color: "hsl(278, 74%, 44%)" },
                            { id: "purple16", color: "hsl(278, 74%, 44%)" },
                            { id: "purple17", color: "hsl(278, 74%, 44%)" },
                            { id: "purple18", color: "hsl(278, 74%, 44%)" },
                            { id: "purple19", color: "hsl(278, 74%, 44%)" },
                            { id: "purple20", color: "hsl(278, 74%, 44%)" },
                            { id: "purple21", color: "hsl(278, 74%, 44%)" },
                            { id: "purple22", color: "hsl(278, 74%, 44%)" },
                            { id: "purple23", color: "hsl(278, 74%, 44%)" },
                            { id: "purple24", color: "hsl(278, 74%, 44%)" },
                            { id: "purple25", color: "hsl(278, 74%, 44%)" },
                            { id: "purple26", color: "hsl(278, 74%, 44%)" },
                            { id: "purple27", color: "hsl(278, 74%, 44%)" },
                            { id: "purple28", color: "hsl(278, 74%, 44%)" },
                            { id: "purple29", color: "hsl(278, 74%, 44%)" },
                            { id: "purple30", color: "hsl(278, 74%, 44%)" },
                            { id: "green1", color: "hsl(134, 71%, 31%)" },
                            { id: "green2", color: "hsl(134, 71%, 31%)" },
                            { id: "green3", color: "hsl(134, 71%, 31%)" },
                            { id: "green4", color: "hsl(134, 71%, 31%)" },
                        ],
                        links: [ {source: "pink1", target: "pink2" , value: 3},
                                 {source: "purple1", target: "purple2" , value: 0.5},
                                 {source: "purple3", target: "purple4" , value: 0.1},
                                 {source: "pink3", target: "pink4" , value: 4}, 
                                 {source: "purple5", target: "purple6" , value: 0.3},
                                 {source: "purple7", target: "purple8" , value: 0.2},
                                 {source: "pink5", target: "pink6" , value: 2.5},
                                 {source: "purple9", target: "purple10" , value: 2.4},
                                 {source: "purple11", target: "purple12" , value: 2.3},
                                 {source: "purple13", target: "purple14" , value: 0.8},
                                 {source: "orange1", target: "orange2" , value: 14},
                                 {source: "orange2", target: "orange3" , value: 3},
                                 {source: "purple15", target: "purple16" , value: 0.1},
                                 {source: "purple17", target: "purple18" , value: 60},
                                 {source: "pink7", target: "pink8" , value: 1},
                                 {source: "purple19", target: "purple20" , value: 0.1},
                                 {source: "orange2", target: "orange3" , value: 14},
                                 {source: "purple19", target: "purple20" , value: 0.4},
                                 {source: "purple21", target: "purple22" , value: 0.4},
                                 {source: "purple23", target: "purple24" , value: 0.5},
                                 {source: "pink9", target: "pink10" , value: 3},
                                 {source: "green1", target: "green2" , value: 0.1},
                                 {source: "purple25", target: "purple26" , value: 0.2},
                                 {source: "pink11", target: "pink12" , value: 0.5},
                                 {source: "pink13", target: "pink14" , value: 14},
                                 {source: "purple27", target: "purple28" , value: 0.4},
                                 {source: "pink15", target: "pink16" , value: 14},
                                 {source: "pink17", target: "pink18" , value: 1},
                                 {source: "green3", target: "green4" , value: 0.1},
                                 ]
                        }
                    }
                margin={{ top: 40, right: 160, bottom: 40, left: 50 }}
                layout="vertical"
                align="justify"
                colors={{ scheme: 'category10' }}
                nodeOpacity={1}
                nodeThickness={40}
                nodeInnerPadding={0}
                nodeSpacing={3}
                nodeBorderWidth={0}
                nodeBorderColor={{ from: 'color', modifiers: [ [ 'darker', 0.8 ] ] }}
                linkOpacity={0.5}
                linkHoverOthersOpacity={0.1}
                enableLinkGradient={true}
                labelPosition="outside"
                labelOrientation="vertical"
                labelPadding={16}
                labelTextColor={{ from: 'color', modifiers: [ [ 'darker', 1 ] ] }}
                animate={true}
                motionStiffness={140}
                motionDamping={13}
            />
        
    }
}

export default Chart;